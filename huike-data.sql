
CREATE DATABASE /*!32312 IF NOT EXISTS*/`huike` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `huike`;

/*Table structure for table `mybatis_review` */

DROP TABLE IF EXISTS `mybatis_review`;

CREATE TABLE `mybatis_review` (
                                  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
                                  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
                                  `age` int(11) NOT NULL COMMENT '年龄',
                                  `sex` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '男' COMMENT '性别',
                                  `create_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '创建人',
                                  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `update_user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '修改人',
                                  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `mybatis_review` */

insert  into `mybatis_review`(`id`,`name`,`age`,`sex`,`create_by`,`create_time`,`update_user`,`update_time`) values (1,'张三',18,'男','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(2,'李四',19,'男','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(4,'王五',20,'男','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(5,'赵六',21,'男','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(6,'孙七',22,'女','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(7,'周八',23,'女','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(8,'吴九',24,'女','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(9,'郑十',25,'女','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(10,'刘一',17,'女','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09'),(11,'陈二',16,'女','1','2022-04-17 18:53:09','1','2022-04-17 18:53:09');

/*Table structure for table `sys_config` */

DROP TABLE IF EXISTS `sys_config`;

CREATE TABLE `sys_config` (
                              `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
                              `config_name` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '参数名称',
                              `config_key` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '参数键名',
                              `config_value` varchar(500) COLLATE utf8_bin DEFAULT '' COMMENT '参数键值',
                              `config_type` char(1) COLLATE utf8_bin DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                              `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                              `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                              `remark` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                              PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='参数配置表';

/*Data for the table `sys_config` */

insert  into `sys_config`(`config_id`,`config_name`,`config_key`,`config_value`,`config_type`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'主框架页-默认皮肤样式名称','sys.index.skinName','skin-blue','Y','admin','2021-03-31 03:12:17','',NULL,'蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow'),(2,'用户管理-账号初始密码','sys.user.initPassword','123456','Y','admin','2021-03-31 03:12:17','',NULL,'初始化密码 123456'),(3,'主框架页-侧边栏主题','sys.index.sideTheme','theme-dark','Y','admin','2021-03-31 03:12:17','',NULL,'深色主题theme-dark，浅色主题theme-light');

/*Table structure for table `sys_dept` */

DROP TABLE IF EXISTS `sys_dept`;

CREATE TABLE `sys_dept` (
                            `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
                            `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
                            `ancestors` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '祖级列表',
                            `dept_name` varchar(30) COLLATE utf8_bin DEFAULT '' COMMENT '部门名称',
                            `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
                            `leader` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '负责人',
                            `phone` varchar(11) COLLATE utf8_bin DEFAULT NULL COMMENT '联系电话',
                            `email` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '邮箱',
                            `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
                            `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                            `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='部门表';

/*Data for the table `sys_dept` */

insert  into `sys_dept`(`dept_id`,`parent_id`,`ancestors`,`dept_name`,`order_num`,`leader`,`phone`,`email`,`status`,`del_flag`,`create_by`,`create_time`,`update_by`,`update_time`) values (100,0,'0','汇客',0,'李峰','15888888888','lifeng@qq.com','0','0','admin','2021-03-31 03:12:09','admin','2022-04-18 10:54:38'),(101,100,'0,100','北京总公司',1,'李峰','15888888888','lifeng@qq.com','0','0','admin','2021-03-31 03:12:09','admin','2022-04-18 10:52:31'),(103,101,'0,100,101','研发部门',2,'李峰','15888888888','lifeng@qq.com','0','0','admin','2021-03-31 03:12:09','admin','2022-04-18 10:52:30'),(214,100,'0,100','销售部门',3,'张三','15011111111','511111111@qq.com','0','0','admin','2021-11-23 15:48:25','admin','2022-04-18 10:52:34'),(215,214,'0,100,214','市场部（线索）',4,'张三','15777777777','zhangsan@qq.com','0','0','admin','2022-04-18 10:51:50','',NULL),(216,214,'0,100,214','销售部（商机）',5,'李四','15666666666','lisi@qq.com','0','0','admin','2022-04-18 10:52:25','',NULL);

/*Table structure for table `sys_dict_data` */

DROP TABLE IF EXISTS `sys_dict_data`;

CREATE TABLE `sys_dict_data` (
                                 `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                 `dict_sort` int(11) DEFAULT '0' COMMENT '字典排序',
                                 `dict_label` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '字典标签',
                                 `dict_value` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '字典键值',
                                 `dict_type` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '字典类型',
                                 `css_class` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                 `list_class` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '表格回显样式',
                                 `is_default` char(1) COLLATE utf8_bin DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
                                 `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                 `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                 `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                                 `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                                 `remark` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                                 PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='字典数据表';

/*Data for the table `sys_dict_data` */

insert  into `sys_dict_data`(`dict_code`,`dict_sort`,`dict_label`,`dict_value`,`dict_type`,`css_class`,`list_class`,`is_default`,`status`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,1,'男','0','sys_user_sex','','','Y','0','admin','2021-03-31 03:12:16','',NULL,'性别男'),(2,2,'女','1','sys_user_sex','','','N','0','admin','2021-03-31 03:12:16','',NULL,'性别女'),(4,1,'显示','0','sys_show_hide','','primary','Y','0','admin','2021-03-31 03:12:16','',NULL,'显示菜单'),(5,2,'隐藏','1','sys_show_hide','','danger','N','0','admin','2021-03-31 03:12:16','',NULL,'隐藏菜单'),(6,1,'正常','0','sys_normal_disable','','primary','Y','0','admin','2021-03-31 03:12:16','',NULL,'正常状态'),(7,2,'停用','1','sys_normal_disable','','danger','N','0','admin','2021-03-31 03:12:16','',NULL,'停用状态'),(8,1,'正常','0','sys_job_status','','primary','Y','0','admin','2021-03-31 03:12:16','',NULL,'正常状态'),(9,2,'暂停','1','sys_job_status','','danger','N','0','admin','2021-03-31 03:12:16','',NULL,'停用状态'),(10,1,'默认','DEFAULT','sys_job_group','','','Y','0','admin','2021-03-31 03:12:16','',NULL,'默认分组'),(11,2,'系统','SYSTEM','sys_job_group','','','N','0','admin','2021-03-31 03:12:16','',NULL,'系统分组'),(12,1,'是','Y','sys_yes_no','','primary','Y','0','admin','2021-03-31 03:12:16','',NULL,'系统默认是'),(13,2,'否','N','sys_yes_no','','danger','N','0','admin','2021-03-31 03:12:16','',NULL,'系统默认否'),(14,1,'通知','1','sys_notice_type','','warning','Y','0','admin','2021-03-31 03:12:16','',NULL,'通知'),(15,2,'公告','2','sys_notice_type','','success','N','0','admin','2021-03-31 03:12:16','',NULL,'公告'),(16,1,'正常','0','sys_notice_status','','primary','Y','0','admin','2021-03-31 03:12:17','',NULL,'正常状态'),(17,2,'关闭','1','sys_notice_status','','danger','N','0','admin','2021-03-31 03:12:17','',NULL,'关闭状态'),(18,1,'新增','1','sys_oper_type','','info','N','0','admin','2021-03-31 03:12:17','',NULL,'新增操作'),(19,2,'修改','2','sys_oper_type','','info','N','0','admin','2021-03-31 03:12:17','',NULL,'修改操作'),(20,3,'删除','3','sys_oper_type','','danger','N','0','admin','2021-03-31 03:12:17','',NULL,'删除操作'),(21,4,'授权','4','sys_oper_type','','primary','N','0','admin','2021-03-31 03:12:17','',NULL,'授权操作'),(22,5,'导出','5','sys_oper_type','','warning','N','0','admin','2021-03-31 03:12:17','',NULL,'导出操作'),(23,6,'导入','6','sys_oper_type','','warning','N','0','admin','2021-03-31 03:12:17','',NULL,'导入操作'),(24,7,'强退','7','sys_oper_type','','danger','N','0','admin','2021-03-31 03:12:17','',NULL,'强退操作'),(25,8,'生成代码','8','sys_oper_type','','warning','N','0','admin','2021-03-31 03:12:17','',NULL,'生成操作'),(26,9,'清空数据','9','sys_oper_type','','danger','N','0','admin','2021-03-31 03:12:17','',NULL,'清空操作'),(27,1,'成功','0','sys_common_status','','primary','N','0','admin','2021-03-31 03:12:17','',NULL,'正常状态'),(28,2,'失败','1','sys_common_status','','danger','N','0','admin','2021-03-31 03:12:17','',NULL,'停用状态'),(100,1,'线上活动','0','clues_item',NULL,NULL,'N','0','admin','2021-04-01 05:58:18','admin','2021-04-02 01:50:30','线上活动'),(101,1,'课程折扣','1','channel_type',NULL,NULL,'N','0','admin','2021-04-01 08:58:56','admin','2021-04-02 01:56:31',NULL),(102,2,'课程代金券','2','channel_type',NULL,NULL,'N','0','admin','2021-04-01 08:59:59','admin','2021-04-02 01:56:41',NULL),(103,2,'推广介绍','1','clues_item',NULL,NULL,'N','0','admin','2021-04-01 09:18:58','admin','2021-04-02 01:50:39','推广介绍'),(104,1,'Java','0','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:39:03','admin','2021-05-11 09:49:12','Java'),(105,2,'前端','1','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:39:36','admin','2021-05-11 09:49:17','前端'),(106,3,'人工智能','2','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:41:04','admin','2021-05-11 09:49:29','人工智能'),(107,4,'大数据','3','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:42:26','admin','2021-05-11 09:49:35','大数据'),(108,5,'Python','4','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:42:27','admin','2021-05-11 09:49:39','Python'),(109,6,'测试','6','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:45:30','admin','2021-04-02 01:55:23','测试'),(110,7,'新媒体','7','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:55:21','admin','2021-04-02 01:55:35','新媒体'),(111,8,'产品经理','8','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:56:46','admin','2021-04-02 01:55:46','产品经理'),(112,9,'UI设计','9','course_subject',NULL,NULL,'N','0','admin','2021-04-01 09:57:22','admin','2021-04-02 01:55:57','UI设计'),(113,1,'小白学员','1','applicable_person',NULL,NULL,'N','0','admin','2021-04-01 10:02:11','admin','2021-04-02 01:52:32','小白学员'),(114,2,'中级程序员','2','applicable_person',NULL,NULL,'N','0','admin','2021-04-01 10:02:38','admin','2021-04-02 01:52:45',NULL),(115,1,'近期学习','0','clues_level',NULL,NULL,'N','0','admin','2021-04-02 01:43:37','admin','2021-05-11 09:47:01','近期学习'),(116,2,'打算学，考虑中','1','clues_level',NULL,NULL,'N','0','admin','2021-04-02 01:44:22','admin','2021-05-11 09:47:07','打算学，考虑中'),(117,3,'进行了解','2','clues_level',NULL,NULL,'N','0','admin','2021-04-02 01:45:22','admin','2021-05-11 09:47:15','进行了解'),(118,4,'打酱油','3','clues_level',NULL,NULL,'N','0','admin','2021-04-02 01:46:24','admin','2021-05-11 09:47:19','打酱油,放弃'),(120,1,'待审核','1','activity_status',NULL,NULL,'N','0','admin','2021-04-02 02:14:33','',NULL,'待审核'),(121,3,'已驳回','3','activity_status',NULL,NULL,'N','0','admin','2021-04-02 02:14:59','admin','2021-04-26 07:05:37','已驳回'),(122,2,'已通过','2','activity_status',NULL,NULL,'N','0','admin','2021-04-02 02:15:28','admin','2021-04-26 07:05:45','通过'),(123,1,'空号','1','reasons_for_reporting',NULL,NULL,'N','0','admin','2021-04-02 08:49:52','',NULL,'空号'),(124,2,'已停机','2','reasons_for_reporting',NULL,NULL,'N','0','admin','2021-04-02 08:50:09','',NULL,'已停机'),(125,3,'广告','3','reasons_for_reporting',NULL,NULL,'N','0','admin','2021-04-02 08:50:31','',NULL,'广告'),(126,4,'竞品','4','reasons_for_reporting',NULL,NULL,'N','0','admin','2021-04-02 08:50:47','',NULL,'竞品'),(127,5,'无法联系','5','reasons_for_reporting',NULL,NULL,'N','0','admin','2021-04-02 08:51:03','',NULL,'无法联系'),(128,6,'其他','6','reasons_for_reporting',NULL,NULL,'N','0','admin','2021-04-02 08:51:26','',NULL,'其他'),(129,1,'创建时间','createTime','clue_rule',NULL,NULL,'N','0','admin','2021-04-08 03:19:48','admin','2021-05-06 06:45:24','创建时间'),(130,2,'渠道来源','channel','clue_rule',NULL,NULL,'N','0','admin','2021-04-08 03:20:21','admin','2021-04-08 03:25:51','渠道来源'),(131,3,'线索意向级别','subject','clue_rule',NULL,NULL,'N','0','admin','2021-04-08 03:21:08','admin','2021-04-08 03:26:01','线索意向级别'),(132,4,'意向等级','level','clue_rule',NULL,NULL,'N','0','admin','2021-04-08 03:24:57','admin','2021-04-08 03:26:12','意向等级'),(133,0,'是','==','clue_rule_createtime',NULL,NULL,'N','0','admin','2021-04-08 03:28:37','admin','2021-05-06 06:33:52','是'),(134,1,'不是','!=','clue_rule_createtime',NULL,NULL,'N','0','admin','2021-04-08 03:28:55','',NULL,'不是'),(135,3,'在之前','<=','clue_rule_createtime',NULL,NULL,'N','0','admin','2021-04-08 03:29:31','admin','2021-06-22 06:26:19','在之前'),(136,4,'在之后','>=','clue_rule_createtime',NULL,NULL,'N','0','admin','2021-04-08 03:29:51','admin','2021-06-22 06:26:30','在之后'),(137,5,'在之间','between','clue_rule_createtime',NULL,NULL,'N','0','admin','2021-04-08 03:30:13','admin','2021-04-08 03:30:32','在之间'),(138,1,'是','==','clue_rule_common',NULL,NULL,'N','0','admin','2021-04-08 03:32:19','admin','2021-04-21 10:22:14','是'),(139,1,'不是','!=','clue_rule_common',NULL,NULL,'N','0','admin','2021-04-08 03:32:36','',NULL,'不是'),(140,3,'包含','in','clue_rule_common',NULL,NULL,'N','0','admin','2021-04-08 03:33:00','admin','2021-04-12 10:27:36','包含'),(141,4,'不包含','not in','clue_rule_common',NULL,NULL,'N','0','admin','2021-04-08 03:33:21','admin','2021-04-12 10:28:07','不包含'),(143,2,'跟进中','2','clue_status',NULL,NULL,'N','0','admin','2021-04-12 10:05:33','admin','2021-05-11 02:01:34','已分配'),(145,0,'用户','0','assign_type',NULL,NULL,'N','0','admin','2021-04-14 08:00:12','',NULL,'用户'),(146,1,'岗位','1','assign_type',NULL,NULL,'N','0','admin','2021-04-14 08:00:21','',NULL,'岗位'),(147,2,'部门','2','assign_type',NULL,NULL,'N','0','admin','2021-04-14 08:00:30','',NULL,'部门'),(148,0,'小时','0','limit_time',NULL,NULL,'N','0','admin','2021-04-16 07:21:09','admin','2021-04-16 07:21:58','小时'),(149,1,'天','1','limit_time',NULL,NULL,'N','0','admin','2021-04-16 07:21:45','admin','2021-04-16 07:22:10','天'),(150,2,'周','2','limit_time',NULL,NULL,'N','0','admin','2021-04-16 07:22:33','',NULL,'周'),(151,0,'小时前','0','warn_time',NULL,NULL,'N','0','admin','2021-04-16 07:24:59','',NULL,'小时前'),(152,1,'天前','1','warn_time',NULL,NULL,'N','0','admin','2021-04-16 07:25:21','',NULL,'天前'),(153,2,'周前','2','warn_time',NULL,NULL,'N','0','admin','2021-04-16 07:25:42','',NULL,'周前'),(154,1,'天以后','1','repeat_get_time',NULL,NULL,'N','0','admin','2021-04-16 07:27:19','admin','2021-09-08 03:14:49','天以后'),(155,1,'周以后','2','repeat_get_time',NULL,NULL,'N','0','admin','2021-04-16 07:27:40','admin','2021-09-08 03:14:55','周以后'),(156,2,'个月以后','3','repeat_get_time',NULL,NULL,'N','0','admin','2021-04-16 07:28:02','admin','2021-09-08 03:14:58','个月以后'),(157,1,'待跟进','1','clue_status',NULL,NULL,'N','0','admin','2021-04-20 10:07:08','',NULL,'待跟进'),(159,4,'已结束','4','activity_status',NULL,NULL,'N','0','admin','2021-04-26 07:05:18','',NULL,NULL),(160,1,'课程','1','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:50:54','',NULL,NULL),(161,2,'价格','2','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:51:10','',NULL,NULL),(162,3,'位置','3','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:51:22','admin','2021-04-28 07:52:55',NULL),(163,4,'时间','4','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:51:33','admin','2021-04-28 07:52:37',NULL),(164,5,'师资','5','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:51:44','admin','2021-04-28 07:55:58',NULL),(165,6,'项目','6','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:51:59','admin','2021-04-28 07:56:12',NULL),(166,7,'薪资','7','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:57:12','',NULL,NULL),(167,8,'职业','8','communication_point',NULL,NULL,'N','0','admin','2021-04-28 07:57:32','',NULL,NULL),(168,0,'无','1','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:38:57','',NULL,NULL),(169,1,'技术','2','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:39:09','',NULL,NULL),(170,2,'产品','3','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:39:18','',NULL,NULL),(171,3,'设计','4','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:39:28','',NULL,NULL),(172,4,'运营','5','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:39:37','',NULL,NULL),(173,5,'市场','6','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:39:48','',NULL,NULL),(174,6,'人事/财务行政','7','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:40:02','',NULL,NULL),(175,7,'销售','8','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:40:14','',NULL,NULL),(176,8,'传媒','9','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:40:29','',NULL,NULL),(177,9,'金融','10','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:40:42','',NULL,NULL),(178,10,'教育培训','11','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:40:54','admin','2021-04-29 03:41:17',NULL),(179,11,'医疗健康','12','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:41:05','',NULL,NULL),(180,13,'供应链/物流','14','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:41:40','admin','2021-04-29 03:41:50',NULL),(181,12,'房地产/建筑','13','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:42:00','',NULL,NULL),(182,14,'采购/贸易','15','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:42:11','',NULL,NULL),(183,15,'咨询/翻译/法律','16','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:42:26','',NULL,NULL),(184,16,'旅游','17','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:42:39','',NULL,NULL),(185,17,'生成制作','18','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:42:50','',NULL,NULL),(186,18,'其他','19','occupation',NULL,NULL,'N','0','admin','2021-04-29 03:43:03','',NULL,NULL),(187,0,'小学','1','education',NULL,NULL,'N','0','admin','2021-04-29 03:43:50','',NULL,NULL),(188,1,'初中','2','education',NULL,NULL,'N','0','admin','2021-04-29 03:43:58','',NULL,NULL),(189,2,'高中','3','education',NULL,NULL,'N','0','admin','2021-04-29 03:44:04','',NULL,NULL),(190,3,'中专','4','education',NULL,NULL,'N','0','admin','2021-04-29 03:44:22','',NULL,NULL),(191,4,'大专','5','education',NULL,NULL,'N','0','admin','2021-04-29 03:44:32','',NULL,NULL),(192,5,'本科','6','education',NULL,NULL,'N','0','admin','2021-04-29 03:44:42','',NULL,NULL),(193,6,'研究生','7','education',NULL,NULL,'N','0','admin','2021-04-29 03:44:52','',NULL,NULL),(194,7,'博士','8','education',NULL,NULL,'N','0','admin','2021-04-29 03:45:02','',NULL,NULL),(195,8,'其他','9','education',NULL,NULL,'N','0','admin','2021-04-29 03:45:15','',NULL,NULL),(196,0,'哲学','1','major',NULL,NULL,'N','0','admin','2021-04-29 05:37:29','',NULL,NULL),(197,1,'经济学','2','major',NULL,NULL,'N','0','admin','2021-04-29 05:37:39','',NULL,NULL),(198,2,'法学','3','major',NULL,NULL,'N','0','admin','2021-04-29 05:37:48','',NULL,NULL),(199,3,'教育学','4','major',NULL,NULL,'N','0','admin','2021-04-29 05:37:58','',NULL,NULL),(200,4,'文学','5','major',NULL,NULL,'N','0','admin','2021-04-29 05:38:05','',NULL,NULL),(201,5,'历史学','6','major',NULL,NULL,'N','0','admin','2021-04-29 05:38:14','',NULL,NULL),(202,6,'理学','7','major',NULL,NULL,'N','0','admin','2021-04-29 05:38:23','',NULL,NULL),(203,7,'工学','8','major',NULL,NULL,'N','0','admin','2021-04-29 05:38:33','',NULL,NULL),(204,8,'农学','9','major',NULL,NULL,'N','0','admin','2021-04-29 05:38:41','',NULL,NULL),(205,9,'医学','10','major',NULL,NULL,'N','0','admin','2021-04-29 05:38:51','',NULL,NULL),(206,10,'管理学','11','major',NULL,NULL,'N','0','admin','2021-04-29 05:39:01','',NULL,NULL),(207,11,'艺术学','12','major',NULL,NULL,'N','0','admin','2021-04-29 05:39:09','',NULL,NULL),(208,0,'待业','1','job',NULL,NULL,'N','0','admin','2021-04-29 05:39:51','',NULL,NULL),(209,1,'在职','2','job',NULL,NULL,'N','0','admin','2021-04-29 05:39:58','',NULL,NULL),(210,2,'已离职','3','job',NULL,NULL,'N','0','admin','2021-04-29 05:40:06','',NULL,NULL),(211,0,'1k-3k','1','salary',NULL,NULL,'N','0','admin','2021-04-29 05:40:51','',NULL,NULL),(212,1,'3k-6k','2','salary',NULL,NULL,'N','0','admin','2021-04-29 05:40:57','',NULL,NULL),(213,2,'6k-10k','3','salary',NULL,NULL,'N','0','admin','2021-04-29 05:41:04','',NULL,NULL),(214,3,'10k-15k','4','salary',NULL,NULL,'N','0','admin','2021-04-29 05:41:11','',NULL,NULL),(215,4,'15k-20k','5','salary',NULL,NULL,'N','0','admin','2021-04-29 05:41:20','',NULL,NULL),(216,5,'20k+','6','salary',NULL,NULL,'N','0','admin','2021-04-29 05:41:27','',NULL,NULL),(217,0,'接通','1','track_status',NULL,NULL,'N','0','admin','2021-04-29 05:42:50','',NULL,NULL),(218,1,'拒绝','2','track_status',NULL,NULL,'N','0','admin','2021-04-29 05:42:56','',NULL,NULL),(219,2,'无人接听','3','track_status',NULL,NULL,'N','0','admin','2021-04-29 05:43:03','',NULL,NULL),(220,0,'用户已报名其他机构','1','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:44:52','',NULL,NULL),(221,1,'用户不感兴趣','2','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:45:00','',NULL,NULL),(222,2,'用户年龄不符','3','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:45:07','',NULL,NULL),(223,3,'用户暂时没有需求','4','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:45:18','',NULL,NULL),(224,4,'学费不满意','5','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:45:26','',NULL,NULL),(225,5,'距离不适合','6','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:45:33','',NULL,NULL),(226,6,'其他','7','reasons_for_business_reporting',NULL,NULL,'N','0','admin','2021-04-29 05:45:44','',NULL,NULL),(227,0,'创建时间','createTime','business_rule',NULL,NULL,'N','0','admin','2021-05-07 01:57:53','',NULL,NULL),(228,1,'商机性质','status','business_rule',NULL,NULL,'N','0','admin','2021-05-07 02:01:25','admin','2021-05-07 02:02:11',NULL),(229,2,'客户地区','region','business_rule',NULL,NULL,'N','0','admin','2021-05-07 02:02:06','',NULL,NULL),(230,3,'意向学科','subject','business_rule',NULL,NULL,'N','0','admin','2021-05-07 02:02:50','admin','2021-05-07 02:04:52',NULL),(231,0,'新增','1','business_status_rule',NULL,NULL,'N','0','admin','2021-05-07 02:09:18','',NULL,NULL),(232,2,'已回收','3','business_status_rule',NULL,NULL,'N','0','admin','2021-05-07 02:09:37','',NULL,NULL),(233,3,'角色','3','assign_type',NULL,NULL,'N','0','admin','2021-05-25 03:47:10','',NULL,NULL);

/*Table structure for table `sys_dict_type` */

DROP TABLE IF EXISTS `sys_dict_type`;

CREATE TABLE `sys_dict_type` (
                                 `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                 `dict_name` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '字典名称',
                                 `dict_type` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '字典类型',
                                 `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                 `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                 `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                                 `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                                 `remark` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                                 PRIMARY KEY (`dict_id`),
                                 UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='字典类型表';

/*Data for the table `sys_dict_type` */

insert  into `sys_dict_type`(`dict_id`,`dict_name`,`dict_type`,`status`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'用户性别','sys_user_sex','0','admin','2021-03-31 03:12:16','admin','2021-05-28 09:35:20','用户性别列表'),(2,'菜单状态','sys_show_hide','0','admin','2021-03-31 03:12:16','',NULL,'菜单状态列表'),(3,'系统开关','sys_normal_disable','0','admin','2021-03-31 03:12:16','',NULL,'系统开关列表'),(4,'任务状态','sys_job_status','0','admin','2021-03-31 03:12:16','',NULL,'任务状态列表'),(5,'任务分组','sys_job_group','0','admin','2021-03-31 03:12:16','',NULL,'任务分组列表'),(6,'系统是否','sys_yes_no','0','admin','2021-03-31 03:12:16','',NULL,'系统是否列表'),(7,'通知类型','sys_notice_type','0','admin','2021-03-31 03:12:16','',NULL,'通知类型列表'),(8,'通知状态','sys_notice_status','0','admin','2021-03-31 03:12:16','',NULL,'通知状态列表'),(9,'操作类型','sys_oper_type','0','admin','2021-03-31 03:12:16','',NULL,'操作类型列表'),(10,'系统状态','sys_common_status','0','admin','2021-03-31 03:12:16','',NULL,'登录状态列表'),(100,'线索跟进项','clues_item','0','admin','2021-04-01 05:57:07','',NULL,'线索跟进项'),(101,'活动类型','channel_type','0','admin','2021-04-01 08:57:57','',NULL,'活动类型'),(102,'课程学科','course_subject','0','admin','2021-04-01 09:37:30','admin','2021-05-28 09:34:51','课程学科'),(103,'适应人群','applicable_person','0','admin','2021-04-01 09:59:41','',NULL,'适应人群'),(104,'线索意向级别','clues_level','0','admin','2021-04-02 01:42:10','admin','2021-04-02 01:42:23','线索意向级别'),(105,'活动状态','activity_status','0','admin','2021-04-02 02:07:22','admin','2021-04-02 02:07:33','活动状态'),(106,' 伪线索上报说明','reasons_for_reporting','0','admin','2021-04-02 08:48:44','admin','2021-05-28 09:36:17','伪线索上报说明'),(107,'线索规则','clue_rule','0','admin','2021-04-08 03:18:14','',NULL,'线索规则'),(108,'线索创建时间表达式','clue_rule_createtime','0','admin','2021-04-08 03:27:42','',NULL,'线索创建时间表达式'),(109,'线索通用表达式','clue_rule_common','0','admin','2021-04-08 03:31:30','admin','2021-04-08 03:31:54','线索通用表达式'),(110,'线索状态','clue_status','0','admin','2021-04-12 10:04:58','',NULL,'线索状态'),(111,'分配类型','assign_type','0','admin','2021-04-14 07:59:39','',NULL,'分配类型'),(112,'回收时限','limit_time','0','admin','2021-04-16 07:20:17','',NULL,'回收时限'),(113,'到期提醒','warn_time','0','admin','2021-04-16 07:24:22','',NULL,'到期提醒'),(114,'重复捞取时间','repeat_get_time','0','admin','2021-04-16 07:26:32','',NULL,'重复捞取时间'),(115,'沟通重点','communication_point','0','admin','2021-04-28 07:50:23','',NULL,NULL),(116,'职业','occupation','0','admin','2021-04-29 03:38:06','',NULL,NULL),(117,'学历','education','0','admin','2021-04-29 03:43:37','',NULL,NULL),(118,'专业','major','0','admin','2021-04-29 05:37:15','',NULL,NULL),(119,'在职情况','job','0','admin','2021-04-29 05:39:42','',NULL,NULL),(120,'薪资','salary','0','admin','2021-04-29 05:40:40','',NULL,NULL),(121,'跟进状态','track_status','0','admin','2021-04-29 05:42:36','',NULL,NULL),(122,'踢回公海原因','reasons_for_business_reporting','0','admin','2021-04-29 05:44:32','',NULL,NULL),(123,'商机规则','business_rule','0','admin','2021-05-07 01:53:52','',NULL,NULL),(124,'商机性质状态','business_status_rule','0','admin','2021-05-07 02:09:02','',NULL,NULL);

/*Table structure for table `sys_logininfor` */

DROP TABLE IF EXISTS `sys_logininfor`;

CREATE TABLE `sys_logininfor` (
                                  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
                                  `user_name` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '用户账号',
                                  `ipaddr` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '登录IP地址',
                                  `login_location` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '登录地点',
                                  `browser` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '浏览器类型',
                                  `os` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '操作系统',
                                  `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
                                  `msg` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '提示消息',
                                  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
                                  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4297 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统访问记录';

/*Data for the table `sys_logininfor` */

insert  into `sys_logininfor`(`info_id`,`user_name`,`ipaddr`,`login_location`,`browser`,`os`,`status`,`msg`,`login_time`) values (4294,'admin','127.0.0.1','内网IP','Chrome 9','Windows 10','0','登录成功','2022-06-06 11:14:13'),(4295,'admin','127.0.0.1','内网IP','Chrome 9','Windows 10','0','退出成功','2022-06-06 11:15:26'),(4296,'lisi','127.0.0.1','内网IP','Chrome 9','Windows 10','0','登录成功','2022-06-06 11:15:32');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
                            `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
                            `menu_name` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
                            `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
                            `order_num` int(11) DEFAULT '0' COMMENT '显示顺序',
                            `path` varchar(200) COLLATE utf8_bin DEFAULT '' COMMENT '路由地址',
                            `component` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '组件路径',
                            `is_frame` int(11) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
                            `is_cache` int(11) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
                            `menu_type` char(1) COLLATE utf8_bin DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
                            `visible` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
                            `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
                            `perms` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '权限标识',
                            `icon` varchar(100) COLLATE utf8_bin DEFAULT '#' COMMENT '菜单图标',
                            `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) COLLATE utf8_bin DEFAULT '' COMMENT '备注',
                            PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2083 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='菜单权限表';

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`menu_id`,`menu_name`,`parent_id`,`order_num`,`path`,`component`,`is_frame`,`is_cache`,`menu_type`,`visible`,`status`,`perms`,`icon`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'系统管理',0,6,'system',NULL,1,0,'M','0','0','','icon_menu_xtgl','admin','2021-03-31 03:12:10','admin','2021-04-15 06:10:52','系统管理目录'),(100,'用户管理',2024,1,'user','system/user/index',1,0,'C','0','0','system:user:list','user','admin','2021-03-31 03:12:10','admin','2021-04-13 02:28:18','用户管理菜单'),(101,'角色管理',2024,2,'role','system/role/index',1,0,'C','0','0','system:role:list','peoples','admin','2021-03-31 03:12:10','admin','2021-04-13 02:28:31','角色管理菜单'),(102,'菜单管理',2024,3,'menu','system/menu/index',1,0,'C','0','0','system:menu:list','tree-table','admin','2021-03-31 03:12:10','admin','2021-04-13 02:28:46','菜单管理菜单'),(103,'部门管理',2024,4,'dept','system/dept/index',1,0,'C','0','0','system:dept:list','tree','admin','2021-03-31 03:12:10','admin','2021-04-13 02:29:00','部门管理菜单'),(104,'岗位管理',2024,5,'post','system/post/index',1,0,'C','0','0','system:post:list','post','admin','2021-03-31 03:12:10','admin','2021-04-13 02:29:13','岗位管理菜单'),(105,'字典管理',1,3,'dict','system/dict/index',1,0,'C','0','0','system:dict:list','icon_menu_zdgl','admin','2021-03-31 03:12:10','admin','2021-04-13 03:34:47','字典管理菜单'),(108,'日志管理',1,9,'log','',1,0,'M','1','0','','log','admin','2021-03-31 03:12:10','admin','2021-04-01 11:04:39','日志管理菜单'),(500,'操作日志',108,10,'operlog','monitor/operlog/index',1,0,'C','0','0','monitor:operlog:list','form','admin','2021-03-31 03:12:11','admin','2021-04-13 02:41:56','操作日志菜单'),(501,'登录日志',108,11,'logininfor','monitor/logininfor/index',1,0,'C','0','0','monitor:logininfor:list','logininfor','admin','2021-03-31 03:12:11','admin','2021-04-13 02:42:04','登录日志菜单'),(1001,'用户查询',100,1,'','',1,0,'F','0','0','system:user:query','#','admin','2021-03-31 03:12:11','',NULL,''),(1002,'用户新增',100,2,'','',1,0,'F','0','0','system:user:add','#','admin','2021-03-31 03:12:11','',NULL,''),(1003,'用户修改',100,3,'','',1,0,'F','0','0','system:user:edit','#','admin','2021-03-31 03:12:11','',NULL,''),(1004,'用户删除',100,4,'','',1,0,'F','0','0','system:user:remove','#','admin','2021-03-31 03:12:11','',NULL,''),(1005,'用户导出',100,5,'','',1,0,'F','0','0','system:user:export','#','admin','2021-03-31 03:12:11','',NULL,''),(1006,'用户导入',100,6,'','',1,0,'F','0','0','system:user:import','#','admin','2021-03-31 03:12:11','',NULL,''),(1007,'重置密码',100,7,'','',1,0,'F','0','0','system:user:resetPwd','#','admin','2021-03-31 03:12:11','',NULL,''),(1008,'角色查询',101,1,'','',1,0,'F','0','0','system:role:query','#','admin','2021-03-31 03:12:11','',NULL,''),(1009,'角色新增',101,2,'','',1,0,'F','0','0','system:role:add','#','admin','2021-03-31 03:12:11','',NULL,''),(1010,'角色修改',101,3,'','',1,0,'F','0','0','system:role:edit','#','admin','2021-03-31 03:12:11','',NULL,''),(1011,'角色删除',101,4,'','',1,0,'F','0','0','system:role:remove','#','admin','2021-03-31 03:12:11','',NULL,''),(1012,'角色导出',101,5,'','',1,0,'F','0','0','system:role:export','#','admin','2021-03-31 03:12:11','',NULL,''),(1013,'菜单查询',102,1,'','',1,0,'F','0','0','system:menu:query','#','admin','2021-03-31 03:12:11','',NULL,''),(1014,'菜单新增',102,2,'','',1,0,'F','0','0','system:menu:add','#','admin','2021-03-31 03:12:11','',NULL,''),(1015,'菜单修改',102,3,'','',1,0,'F','0','0','system:menu:edit','#','admin','2021-03-31 03:12:11','',NULL,''),(1016,'菜单删除',102,4,'','',1,0,'F','0','0','system:menu:remove','#','admin','2021-03-31 03:12:11','',NULL,''),(1017,'部门查询',103,1,'','',1,0,'F','0','0','system:dept:query','#','admin','2021-03-31 03:12:11','',NULL,''),(1018,'部门新增',103,2,'','',1,0,'F','0','0','system:dept:add','#','admin','2021-03-31 03:12:11','',NULL,''),(1019,'部门修改',103,3,'','',1,0,'F','0','0','system:dept:edit','#','admin','2021-03-31 03:12:11','',NULL,''),(1020,'部门删除',103,4,'','',1,0,'F','0','0','system:dept:remove','#','admin','2021-03-31 03:12:11','',NULL,''),(1021,'岗位查询',104,1,'','',1,0,'F','0','0','system:post:query','#','admin','2021-03-31 03:12:11','',NULL,''),(1022,'岗位新增',104,2,'','',1,0,'F','0','0','system:post:add','#','admin','2021-03-31 03:12:11','',NULL,''),(1023,'岗位修改',104,3,'','',1,0,'F','0','0','system:post:edit','#','admin','2021-03-31 03:12:11','',NULL,''),(1024,'岗位删除',104,4,'','',1,0,'F','0','0','system:post:remove','#','admin','2021-03-31 03:12:12','',NULL,''),(1025,'岗位导出',104,5,'','',1,0,'F','0','0','system:post:export','#','admin','2021-03-31 03:12:12','',NULL,''),(1026,'字典查询',105,1,'#','',1,0,'F','0','0','system:dict:query','#','admin','2021-03-31 03:12:12','',NULL,''),(1027,'字典新增',105,2,'#','',1,0,'F','0','0','system:dict:add','#','admin','2021-03-31 03:12:12','',NULL,''),(1028,'字典修改',105,3,'#','',1,0,'F','0','0','system:dict:edit','#','admin','2021-03-31 03:12:12','',NULL,''),(1029,'字典删除',105,4,'#','',1,0,'F','0','0','system:dict:remove','#','admin','2021-03-31 03:12:12','',NULL,''),(1030,'字典导出',105,5,'#','',1,0,'F','0','0','system:dict:export','#','admin','2021-03-31 03:12:12','',NULL,''),(1040,'操作查询',500,1,'#','',1,0,'F','0','0','monitor:operlog:query','#','admin','2021-03-31 03:12:12','',NULL,''),(1041,'操作删除',500,2,'#','',1,0,'F','0','0','monitor:operlog:remove','#','admin','2021-03-31 03:12:12','',NULL,''),(1042,'日志导出',500,4,'#','',1,0,'F','0','0','monitor:operlog:export','#','admin','2021-03-31 03:12:12','',NULL,''),(1043,'登录查询',501,1,'#','',1,0,'F','0','0','monitor:logininfor:query','#','admin','2021-03-31 03:12:12','',NULL,''),(1044,'登录删除',501,2,'#','',1,0,'F','0','0','monitor:logininfor:remove','#','admin','2021-03-31 03:12:12','',NULL,''),(1045,'日志导出',501,3,'#','',1,0,'F','0','0','monitor:logininfor:export','#','admin','2021-03-31 03:12:12','',NULL,''),(2001,'商机管理',0,2,'business','clues/business/index',1,0,'C','0','0','business:business:list','icon_menu_sjgl','admin','2021-03-31 09:39:34','admin','2021-04-27 09:34:06',''),(2002,'合同管理',0,3,'contract','clues/contract/index',1,0,'C','0','0','contract:contract:list','icon_menu_htgl','admin','2021-03-31 09:40:27','admin','2021-05-21 07:35:07',''),(2003,'活动管理',0,4,'activity','clues/activity/index',1,0,'C','0','0','clues:activity:list','icon_menu_hdgl','admin','2021-04-01 02:56:38','admin','2021-04-27 03:21:08','活动管理菜单'),(2004,'活动管理查询',2003,1,'#','',1,0,'F','0','0','clues:activity:query','#','admin','2021-04-01 02:56:38','admin','2021-04-01 03:08:39',''),(2005,'活动管理新增',2003,2,'#','',1,0,'F','0','0','clues:activity:add','#','admin','2021-04-01 02:56:38','admin','2021-04-01 03:08:50',''),(2006,'活动管理修改',2003,3,'#','',1,0,'F','0','0','clues:activity:edit','#','admin','2021-04-01 02:56:38','admin','2021-04-01 03:09:04',''),(2007,'活动管理删除',2003,4,'#','',1,0,'F','0','0','clues:activity:remove','#','admin','2021-04-01 02:56:38','admin','2021-04-01 03:09:14',''),(2008,'活动管理导出',2003,5,'#','',1,0,'F','0','0','clues:activity:export','#','admin','2021-04-01 02:56:38','admin','2021-04-01 03:09:31',''),(2009,'课程管理',0,4,'course','clues/course/index',1,0,'C','0','0','clues:course:list','icon_menu_kcgl','admin','2021-04-01 10:14:16','admin','2021-04-13 02:04:05','课程管理菜单'),(2010,'课程管理查询',2009,1,'#','',1,0,'F','0','0','clues:course:query','#','admin','2021-04-01 10:14:16','',NULL,''),(2011,'课程管理新增',2009,2,'#','',1,0,'F','0','0','clues:course:add','#','admin','2021-04-01 10:14:16','',NULL,''),(2012,'课程管理修改',2009,3,'#','',1,0,'F','0','0','clues:course:edit','#','admin','2021-04-01 10:14:16','',NULL,''),(2013,'课程管理删除',2009,4,'#','',1,0,'F','0','0','clues:course:remove','#','admin','2021-04-01 10:14:16','',NULL,''),(2015,'线索管理',0,1,'clue','clues/clue/index',1,0,'C','0','0','clues:clue:list','icon_menu_xsgl','admin','2021-04-02 09:02:31','admin','2021-04-13 02:03:06','线索管理菜单'),(2016,'线索管理查询',2015,1,'#','',1,0,'F','0','0','clues:clue:query','#','admin','2021-04-02 09:02:31','',NULL,''),(2017,'线索管理新增',2015,2,'#','',1,0,'F','0','0','clues:clue:add','#','admin','2021-04-02 09:02:31','',NULL,''),(2018,'线索管理跟进',2015,3,'#','',1,0,'F','0','0','clues:record:add','#','admin','2021-04-02 09:02:31','admin','2021-05-19 09:28:18',''),(2019,'线索管理删除',2015,4,'#','',1,0,'F','0','0','clues:clue:remove','#','admin','2021-04-02 09:02:31','',NULL,''),(2022,'线索管理查看',2015,6,'',NULL,1,0,'F','0','0','clues:clue:info','#','admin','2021-04-08 10:07:48','',NULL,''),(2023,'线索管理分配',2015,7,'',NULL,1,0,'F','0','0','clues:clue:assignment','#','admin','2021-04-08 10:08:49','admin','2021-05-19 09:27:49',''),(2024,'权限管理',1,2,'permission',NULL,1,0,'M','0','0',NULL,'icon_menu_qxgl','admin','2021-04-13 02:27:50','',NULL,''),(2025,'线索配置',1,4,'clew','system/clew/index',1,0,'C','0','0','system:clew:list','icon_menu_xspz','admin','2021-04-13 03:11:34','admin','2021-04-13 03:32:24',''),(2026,'商机配置',1,6,'opportunity','system/opportunity/index',1,0,'C','0','0','system:opportunity:list','icon_menu_sjpz','admin','2021-04-13 03:34:06','admin','2021-04-26 08:16:57',''),(2027,'统计分析',0,5,'countAnalysis','clues/countAnalysis/index',1,0,'C','0','0','clues:countAnalysis:list','icon_menu_tjfx','admin','2021-04-15 06:06:56','admin','2021-11-05 08:56:10',''),(2028,'商机管理查询',2001,1,'',NULL,1,0,'F','0','0','business:business:query','#','admin','2021-04-26 05:40:08','admin','2021-04-27 09:49:30',''),(2029,'商机管理新增',2001,2,'',NULL,1,0,'F','0','0','business:business:add','#','admin','2021-04-26 05:40:46','admin','2021-04-27 09:49:37',''),(2031,'商机管理查看',2001,4,'',NULL,1,0,'F','0','0','business:business:info','#','admin','2021-04-26 05:42:03','admin','2021-04-27 09:49:56',''),(2032,'商机管理分配',2001,5,'',NULL,1,0,'F','0','0','business:business:assignment','#','admin','2021-04-26 05:42:29','admin','2021-05-19 09:11:29',''),(2033,'活动管理通过',2003,6,'',NULL,1,0,'F','0','0','clues:activity:pass','#','admin','2021-04-26 06:47:54','admin','2021-05-31 10:25:41',''),(2034,'通知中心',1,8,'noticeCenter','system/noticeCenter/index',1,0,'C','0','0','system:noticeCenter:list','icon_menu_tzzx','admin','2021-04-26 07:04:59','admin','2021-04-26 07:05:33',''),(2035,'转派管理',0,5,'transferManage','clues/transferManage/index',1,0,'C','0','0','transfer:transfer:list','example','admin','2021-04-26 10:09:13','admin','2021-11-05 08:56:03',''),(2042,'系统日志',1,9,'systemLog','system/systemLog/index',1,0,'C','0','0','system:systemLog:list','icon_menu_xtrz','admin','2021-04-28 10:04:59','',NULL,''),(2043,'商机跟进',2001,6,'',NULL,1,0,'F','0','0','business:record:add','#','admin','2021-05-14 07:16:18','admin','2021-05-14 07:18:29',''),(2044,'踢回公海',2001,8,'',NULL,1,0,'F','0','0','business:business:back','#','admin','2021-05-14 07:18:11','admin','2021-05-14 07:18:44',''),(2045,'查询公海池',2001,9,'',NULL,1,0,'F','0','0','business:business:pool','#','admin','2021-05-14 07:26:41','admin','2021-05-14 07:32:35',''),(2046,'线索池',2015,8,'',NULL,1,0,'F','0','0','clues:clue:pool','#','admin','2021-05-14 07:32:07','admin','2021-05-14 07:33:00',''),(2047,'线索跟进记录',2015,9,'',NULL,1,0,'F','0','0','clues:record:list','#','admin','2021-05-17 02:47:10','',NULL,''),(2048,'线索管理批量添加',2015,10,'',NULL,1,0,'F','0','0','clues:clue:batchAdd','#','admin','2021-05-19 09:04:37','admin','2021-05-19 09:05:27',''),(2049,'商机捞取',2001,10,'',NULL,1,0,'F','0','0','business:business:gain','#','admin','2021-05-19 09:13:40','',NULL,''),(2051,'活动查看',2001,11,'',NULL,1,0,'F','0','0','clues:activity:list','#','admin','2021-05-20 01:46:35','',NULL,''),(2052,'合同查询',2002,1,'',NULL,1,0,'F','0','0','contract:contract:query','#','admin','2021-05-21 07:35:53','admin','2021-05-21 07:36:30',''),(2053,'合同新增',2002,2,'',NULL,1,0,'F','0','0','contract:contract:add','#','admin','2021-05-21 07:36:23','',NULL,''),(2059,'线索捞取',2015,11,'clues:clue:gain',NULL,1,0,'F','0','0','clues:clue:gain','#','admin','2021-05-25 06:19:10','admin','2021-05-25 06:19:27',''),(2065,'活动管理驳回',2003,7,'',NULL,1,0,'F','0','0','clues:activity:reject','#','admin','2021-05-31 10:26:18','',NULL,''),(2066,'商机转合同',2001,14,'',NULL,1,0,'F','0','0','contract:contract:change','#','admin','2021-06-01 09:21:16','',NULL,''),(2067,'合同基本信息查看',2002,10,'',NULL,1,0,'F','0','0','contract:contract:detail','#','admin','2021-06-02 07:44:27','',NULL,''),(2068,'首页',0,0,'index','indexHome',1,0,'C','0','0','indexHome:list','icon_menu_home','admin','2021-06-23 08:34:29','admin','2021-07-01 07:11:01',''),(2069,'首页基础数据查询',2068,1,'',NULL,1,0,'F','0','0','indexHome:baseQuery','#','admin','2021-06-23 08:59:31','',NULL,''),(2070,'首页今日简报',2068,2,'',NULL,1,0,'F','0','0','indexHome:todayQuery','#','admin','2021-06-23 09:00:26','',NULL,''),(2071,'首页待办',2068,3,'',NULL,1,0,'F','0','0','indexHome:todoQuery','#','admin','2021-06-23 09:01:35','',NULL,''),(2072,'首页漏斗图',2068,4,'',NULL,1,0,'F','0','0','indexHome:funnelQuery','#','admin','2021-06-23 09:02:13','',NULL,''),(2073,'首页销售龙虎榜',2068,5,'',NULL,1,0,'F','0','0','indexHome:saleQuery','#','admin','2021-06-23 09:02:48','',NULL,''),(2074,'首页商机龙虎榜',2068,6,'',NULL,1,0,'F','0','0','indexHome:businessQuery','#','admin','2021-06-23 09:03:32','',NULL,''),(2077,'线索转商机',2015,12,'clues:clue:changeBusiness',NULL,1,0,'F','0','0','clues:clue:changeBusiness','#','admin','2021-06-23 09:03:32','admin','2021-06-23 09:03:32','线索转商机'),(2078,'转派处理',2035,0,'transfer:transfer:assignment',NULL,1,0,'F','0','0','transfer:transfer:assignment','#','admin','2021-06-23 09:03:32','admin','2021-06-23 09:03:32','转派管理'),(2079,'商机跟进记录列表查询',2001,6,'business:record:list',NULL,1,0,'F','0','0','business:record:list','#','admin','2021-06-23 09:02:48','',NULL,'商机跟进记录'),(2080,'合同详情预览',2002,6,'contract:contract:info',NULL,1,0,'F','0','0','contract:contract:info','#','admin','2021-06-23 09:02:48','admin','2021-06-23 09:02:48','合同详情'),(2081,'退回公海',2001,11,'business:business:back',NULL,1,0,'F','0','0','business:business:back','#','admin','2021-06-23 09:02:48','admin','2021-06-23 09:02:48','退回公海'),(2082,'伪线索',2015,8,'clues:clue:false',NULL,1,0,'F','0','0','clues:clue:false','#','admin','2021-06-23 09:02:48','admin','2021-06-23 09:02:48','伪线索');

/*Table structure for table `sys_notice` */

DROP TABLE IF EXISTS `sys_notice`;

CREATE TABLE `sys_notice` (
                              `notice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
                              `notice_title` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '公告标题',
                              `notice_type` char(1) COLLATE utf8_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
                              `notice_content` longblob COMMENT '公告内容',
                              `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
                              `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                              `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                              `remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                              `notice_user_id` bigint(20) DEFAULT NULL,
                              PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='通知公告表';

/*Data for the table `sys_notice` */

/*Table structure for table `sys_oper_log` */

DROP TABLE IF EXISTS `sys_oper_log`;

CREATE TABLE `sys_oper_log` (
                                `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
                                `title` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '模块标题',
                                `business_type` int(11) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
                                `method` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '方法名称',
                                `request_method` varchar(10) COLLATE utf8_bin DEFAULT '' COMMENT '请求方式',
                                `operator_type` int(11) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
                                `oper_name` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '操作人员',
                                `dept_name` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '部门名称',
                                `oper_url` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '请求URL',
                                `oper_ip` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '主机地址',
                                `oper_location` varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '操作地点',
                                `oper_param` varchar(2000) COLLATE utf8_bin DEFAULT '' COMMENT '请求参数',
                                `json_result` varchar(2000) COLLATE utf8_bin DEFAULT '' COMMENT '返回参数',
                                `status` int(11) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
                                `error_msg` varchar(2000) COLLATE utf8_bin DEFAULT '' COMMENT '错误消息',
                                `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
                                PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21250 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='操作日志记录';

/*Data for the table `sys_oper_log` */

/*Table structure for table `sys_post` */

DROP TABLE IF EXISTS `sys_post`;

CREATE TABLE `sys_post` (
                            `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
                            `post_code` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '岗位编码',
                            `post_name` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '岗位名称',
                            `post_sort` int(11) NOT NULL COMMENT '显示顺序',
                            `status` char(1) COLLATE utf8_bin NOT NULL COMMENT '状态（0正常 1停用）',
                            `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                            PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='岗位信息表';

/*Data for the table `sys_post` */

insert  into `sys_post`(`post_id`,`post_code`,`post_name`,`post_sort`,`status`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'ceo','董事长',1,'0','admin','2021-03-31 03:12:10','',NULL,''),(2,'se','项目经理',2,'0','admin','2021-03-31 03:12:10','',NULL,''),(3,'hr','人力资源',3,'0','admin','2021-03-31 03:12:10','',NULL,''),(4,'user','普通员工',4,'0','admin','2021-03-31 03:12:10','admin','2021-11-16 14:26:17',''),(11,'XYG001','新员工',5,'0','admin','2021-11-19 16:15:48','',NULL,NULL),(12,'XSBZG','销售部主管',10,'0','admin','2021-11-23 15:55:44','',NULL,NULL),(13,'XSBZY','销售部专员',13,'0','admin','2021-11-23 15:56:10','',NULL,NULL);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
                            `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                            `role_name` varchar(30) COLLATE utf8_bin NOT NULL COMMENT '角色名称',
                            `role_key` varchar(100) COLLATE utf8_bin NOT NULL COMMENT '角色权限字符串',
                            `role_sort` int(11) NOT NULL COMMENT '显示顺序',
                            `data_scope` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
                            `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
                            `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
                            `status` char(1) COLLATE utf8_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
                            `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                            `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                            PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色信息表';

/*Data for the table `sys_role` */

insert  into `sys_role`(`role_id`,`role_name`,`role_key`,`role_sort`,`data_scope`,`menu_check_strictly`,`dept_check_strictly`,`status`,`del_flag`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,'超级管理员','admin',1,'1',1,1,'0','0','admin','2021-03-31 03:12:10','',NULL,'超级管理员'),(2,'市场专员（线索）','xiansuo',2,'5',1,1,'0','0','admin','2022-04-18 09:20:37','admin','2022-04-19 23:42:05',NULL),(3,'销售专员(商机)','shangji',3,'5',1,1,'0','0','admin','2022-04-18 09:21:03','admin','2022-04-19 23:42:28',NULL),(4,'销售主管(管理所有销售)','zhuguan',4,'4',1,1,'0','0','admin','2022-04-18 09:21:29','admin','2022-04-19 15:21:09',NULL);

/*Table structure for table `sys_role_dept` */

DROP TABLE IF EXISTS `sys_role_dept`;

CREATE TABLE `sys_role_dept` (
                                 `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                 `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
                                 PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色和部门关联表';

/*Data for the table `sys_role_dept` */

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
                                 `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                 `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
                                 PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色和菜单关联表';

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`role_id`,`menu_id`) values (2,1),(2,2015),(2,2016),(2,2017),(2,2018),(2,2022),(2,2034),(2,2046),(2,2047),(2,2059),(2,2068),(2,2069),(2,2070),(2,2071),(2,2072),(2,2073),(2,2074),(2,2077),(2,2082),(3,1),(3,2001),(3,2002),(3,2028),(3,2029),(3,2031),(3,2034),(3,2043),(3,2044),(3,2045),(3,2049),(3,2051),(3,2052),(3,2053),(3,2066),(3,2067),(3,2068),(3,2069),(3,2070),(3,2071),(3,2072),(3,2073),(3,2074),(3,2079),(3,2080),(3,2081),(4,1),(4,2001),(4,2002),(4,2003),(4,2004),(4,2005),(4,2006),(4,2007),(4,2008),(4,2009),(4,2010),(4,2011),(4,2012),(4,2013),(4,2015),(4,2016),(4,2017),(4,2018),(4,2019),(4,2022),(4,2023),(4,2027),(4,2028),(4,2029),(4,2031),(4,2032),(4,2033),(4,2034),(4,2035),(4,2043),(4,2044),(4,2045),(4,2046),(4,2047),(4,2048),(4,2049),(4,2051),(4,2052),(4,2053),(4,2059),(4,2065),(4,2067),(4,2068),(4,2069),(4,2070),(4,2071),(4,2072),(4,2073),(4,2074),(4,2078),(4,2079),(4,2080);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
                            `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                            `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
                            `user_name` varchar(30) COLLATE utf8_bin NOT NULL COMMENT '用户账号',
                            `nick_name` varchar(30) COLLATE utf8_bin NOT NULL COMMENT '用户昵称',
                            `user_type` varchar(2) COLLATE utf8_bin DEFAULT '00' COMMENT '用户类型（00系统用户）',
                            `email` varchar(50) COLLATE utf8_bin DEFAULT '' COMMENT '用户邮箱',
                            `phonenumber` varchar(11) COLLATE utf8_bin DEFAULT '' COMMENT '手机号码',
                            `sex` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
                            `avatar` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '头像地址',
                            `password` varchar(100) COLLATE utf8_bin DEFAULT '' COMMENT '密码',
                            `status` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
                            `del_flag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                            `login_ip` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '最后登录IP',
                            `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
                            `create_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '创建者',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8_bin DEFAULT '' COMMENT '更新者',
                            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                            PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1028 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户信息表';

/*Data for the table `sys_user` */

insert  into `sys_user`(`user_id`,`dept_id`,`user_name`,`nick_name`,`user_type`,`email`,`phonenumber`,`sex`,`avatar`,`password`,`status`,`del_flag`,`login_ip`,`login_date`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) values (1,103,'admin','admin','00','admin@163.com','15888888888','1','','$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2','0','0','127.0.0.1','2021-03-31 03:12:10','admin','2021-03-31 03:12:10','',NULL,'管理员'),(2,215,'zhangsan','zhangsan','00','zhangsan@qq.com','15777777777','0','','$2a$10$LrJqsVulcZo9Ia.dHSt02.IiB/4u/W5D3e7kSvEuV1/rW0nDb3wOu','0','0','',NULL,'admin','2022-04-18 11:29:20','admin','2022-04-18 11:46:54',NULL),(3,215,'zhangsan1','zhangsan1','00','zhangsan1@qq.com','15333333333','0','','$2a$10$edSLagLw.q5QvLGTPP6nGulNR1ZWEQ4ar8BSCZNNwGeYfOyMKcLq6','0','0','',NULL,'admin','2022-04-18 11:32:21','admin','2022-04-18 11:47:22',NULL),(4,216,'lisi','lisi','00','lisi@qq.com','15666666666','1','','$2a$10$3Fxg5AOywhh4MbgFLkYJE.i.QSA1VKSJVZbMI//pwsZ5boaqaRAf.','0','0','',NULL,'admin','2022-04-18 11:30:18','admin','2022-04-18 11:47:28',NULL),(5,216,'lisi1','lisi1','00','lisi1@qq.com','15444444444','0','','$2a$10$foqej.Z0e.otPwinDGAtDOFu6HzEkPDesHjh76XNWQx52wGzD3uRy','0','0','',NULL,'admin','2022-04-18 11:33:28','admin','2022-04-18 11:47:32',NULL),(6,214,'lifeng','lifeng','00','lifeng@qq.com','15555555555','0','','$2a$10$iC36e6hFVpa1hGX.fTYJL.swWrrukDUCyv5Zl.Jl00tL1IXWQgCCy','0','0','',NULL,'admin','2022-04-18 11:31:20','admin','2022-04-18 11:47:38',NULL);

/*Table structure for table `sys_user_post` */

DROP TABLE IF EXISTS `sys_user_post`;

CREATE TABLE `sys_user_post` (
                                 `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                 `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
                                 PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户与岗位关联表';

/*Data for the table `sys_user_post` */

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
                                 `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                 `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                 PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户和角色关联表';

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`user_id`,`role_id`) values (1,1),(2,2),(3,2),(4,3),(5,3),(6,4),(7,2),(1022,2),(1023,3),(1024,4),(1025,2),(1026,3),(1027,2);

/*Table structure for table `tb_activity` */

DROP TABLE IF EXISTS `tb_activity`;

CREATE TABLE `tb_activity` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
                               `name` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '活动名称',
                               `channel` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '渠道来源',
                               `info` varchar(512) COLLATE utf8_bin NOT NULL COMMENT '活动简介',
                               `type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '活动类型',
                               `discount` float DEFAULT NULL COMMENT '璇剧▼鎶樻墸',
                               `vouchers` int(11) DEFAULT NULL COMMENT '课程代金券',
                               `status` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '状态',
                               `begin_time` timestamp NULL DEFAULT NULL COMMENT '开始时间',
                               `end_time` timestamp NULL DEFAULT NULL COMMENT '结束时间',
                               `create_time` timestamp NULL DEFAULT NULL,
                               `code` varchar(32) COLLATE utf8_bin DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='活动管理';

/*Data for the table `tb_activity` */

insert  into `tb_activity`(`id`,`name`,`channel`,`info`,`type`,`discount`,`vouchers`,`status`,`begin_time`,`end_time`,`create_time`,`code`) values (72,'推广拉新活动','1','拉新','2',NULL,20,'2','2021-11-05 08:00:00','2022-01-01 07:59:00','2021-11-05 11:06:28','fea1wzqg'),(75,'教师节活动','0','教师节活动','2',NULL,333,'2','2021-11-05 08:00:00','2021-11-27 07:59:00','2021-11-05 22:02:22','p8jpsl9g'),(77,'双十二快消活动','0','快速拉取新客户','2',NULL,555,'2','2021-11-05 08:00:00','2021-11-28 07:59:00','2021-11-05 22:38:28','pa35elxs'),(78,'新春大促活动0.1折','1','新春天大促0.1折','1',0.1,NULL,'2','2022-01-04 08:00:00','2022-03-01 07:59:00','2021-11-08 22:17:04','a1a2sj8o'),(110,'双11打折','0','双11打折','1',8.5,NULL,'2','2021-11-23 00:00:00','2021-12-31 23:59:00','2021-11-23 18:49:58','3c3nechs'),(116,'持续优惠','1','三年之内持续优惠','1',9.5,NULL,'2','2022-06-06 00:00:00','2026-12-31 23:59:00','2022-06-06 10:40:39','oni1v5sd');

/*Table structure for table `tb_assign_record` */

DROP TABLE IF EXISTS `tb_assign_record`;

CREATE TABLE `tb_assign_record` (
                                    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
                                    `assign_id` bigint(20) DEFAULT NULL COMMENT '关联id',
                                    `user_id` bigint(20) DEFAULT NULL COMMENT '所属人用户id',
                                    `user_name` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '所属人名称',
                                    `dept_id` bigint(20) DEFAULT NULL COMMENT '所属人所属组织',
                                    `create_time` timestamp NULL DEFAULT NULL COMMENT '分配时间',
                                    `create_by` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '分配人',
                                    `latest` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '是否当前最新分配人',
                                    `type` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '类型0 线索 1 商机',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='分配记录表';

/*Data for the table `tb_assign_record` */

insert  into `tb_assign_record`(`id`,`assign_id`,`user_id`,`user_name`,`dept_id`,`create_time`,`create_by`,`latest`,`type`) values (1,40,2,'zhangsan',215,'2022-06-04 01:07:26','lifeng','1','0'),(2,2,2,'zhangsan',215,'2022-06-04 01:07:26','lifeng','0','0'),(3,4,2,'zhaoliu',215,'2022-06-04 01:07:26','lifeng','0','0'),(4,4,2,'zhangsan',215,'2022-06-04 01:07:26','lifeng','1','0'),(5,5,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(6,6,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(7,7,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(8,8,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(9,9,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(10,10,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(11,11,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(12,12,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(13,13,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(14,14,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(15,15,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(16,16,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(17,17,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(18,18,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(19,19,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(20,20,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(21,21,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(22,22,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(23,23,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(24,24,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(25,25,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(26,26,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(27,27,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(28,28,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(29,29,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(30,30,2,'zhangsan',215,'2022-06-04 01:07:27','lifeng','1','0'),(31,31,2,'zhangsan',215,'2022-06-04 01:07:46','lifeng','1','0'),(32,32,2,'zhangsan',215,'2022-06-04 01:07:46','lifeng','1','0'),(33,33,2,'zhangsan',215,'2022-06-04 01:07:46','lifeng','1','0'),(34,34,2,'zhangsan',215,'2022-06-04 01:07:46','lifeng','1','0'),(35,35,2,'zhangsan',215,'2022-06-04 01:07:46','lifeng','1','0'),(36,36,2,'zhangsan',215,'2022-06-04 01:07:46','lifeng','1','0'),(37,2,2,'zhangsan',215,'2022-06-04 01:08:51','zhangsan','1','0'),(38,3,2,'zhangsan',215,'2022-06-04 01:10:33','zhangsan','1','0'),(39,3449,1,'admin',103,'2022-06-06 11:05:38','admin','1','1'),(40,3450,1,'admin',103,'2022-06-06 11:09:31','admin','0','1'),(41,3451,1,'admin',103,'2022-06-06 11:10:29','admin','0','1'),(42,3452,1,'admin',103,'2022-06-06 11:11:05','admin','0','1'),(43,3453,1,'admin',103,'2022-06-06 11:12:34','admin','0','1'),(44,3453,4,'lisi',216,'2022-06-06 11:14:58','admin','1','1'),(45,3452,4,'lisi',216,'2022-06-06 11:15:08','admin','1','1'),(46,3451,4,'lisi',216,'2022-06-06 11:15:15','admin','1','1'),(47,3450,4,'lisi',216,'2022-06-06 11:15:21','admin','1','1');

/*Table structure for table `tb_business` */

DROP TABLE IF EXISTS `tb_business`;

CREATE TABLE `tb_business` (
                               `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商机id',
                               `name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '客户姓名',
                               `phone` varchar(11) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
                               `channel` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '渠道',
                               `activity_id` bigint(20) DEFAULT NULL COMMENT '活动id',
                               `provinces` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '省',
                               `city` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '区',
                               `sex` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '性別',
                               `age` int(11) DEFAULT NULL COMMENT '年龄',
                               `weixin` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '微信',
                               `qq` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'qq',
                               `level` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '意向等级',
                               `subject` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '意向学科',
                               `course_id` bigint(20) DEFAULT NULL COMMENT '课程',
                               `create_by` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
                               `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
                               `occupation` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '职业',
                               `education` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '学历',
                               `job` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '在职情况',
                               `salary` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '薪资',
                               `major` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '专业',
                               `expected_salary` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '希望薪资',
                               `reasons` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '学习原因',
                               `plan` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '职业计划',
                               `plan_time` timestamp NULL DEFAULT NULL COMMENT '计划时间',
                               `other_intention` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '其他意向',
                               `status` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '状态(已分配1  进行中2  回收3)',
                               `next_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                               `last_update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                               `clue_id` bigint(20) DEFAULT NULL,
                               `transfer` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '是否转派',
                               `remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
                               `end_time` timestamp NULL DEFAULT NULL COMMENT '回收时间',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3454 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='商机';

/*Data for the table `tb_business` */

insert  into `tb_business`(`id`,`name`,`phone`,`channel`,`activity_id`,`provinces`,`city`,`sex`,`age`,`weixin`,`qq`,`level`,`subject`,`course_id`,`create_by`,`create_time`,`occupation`,`education`,`job`,`salary`,`major`,`expected_salary`,`reasons`,`plan`,`plan_time`,`other_intention`,`status`,`next_time`,`last_update_time`,`clue_id`,`transfer`,`remark`,`end_time`) values (3449,'何仙姑','13100131000','0',NULL,'北京市','市辖区','1',22,'wx13100131000','3100131000',NULL,'0',NULL,'admin','2022-06-06 11:05:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,'2022-06-06 11:05:38',NULL,'0',NULL,'2022-06-06 14:05:38'),(3450,'张国老','13200132000','1',NULL,'山西省','太原市','0',NULL,'wx13200132000','3200132000',NULL,'0',NULL,'admin','2022-06-06 11:09:31',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,'2022-06-06 11:15:20',NULL,'0',NULL,'2022-06-06 14:15:21'),(3451,'吕洞宾','13300133000','0',NULL,'山西省','太原市','0',NULL,'wx13300133000','3300133000',NULL,'1',NULL,'admin','2022-06-06 11:10:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4',NULL,'2022-06-06 11:16:21',NULL,'0',NULL,'2022-06-06 14:15:15'),(3452,'韩湘子','13400134000','0',NULL,'山西省','太原市','0',22,'wx13400134000','3400134000',NULL,'1',NULL,'admin','2022-06-06 11:11:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4',NULL,'2022-06-06 11:16:12',NULL,'0',NULL,'2022-06-06 14:15:08'),(3453,'汉钟离','13500135000','0',NULL,'山西省','长治市','0',33,'wx13500135000','3500135000',NULL,'1',NULL,'admin','2022-06-06 11:12:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4',NULL,'2022-06-06 11:16:02',NULL,'0',NULL,'2022-06-06 14:14:58');


/*Table structure for table `tb_clue` */

DROP TABLE IF EXISTS `tb_clue`;

CREATE TABLE `tb_clue` (
                           `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '线索id',
                           `name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '客户姓名',
                           `phone` varchar(11) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
                           `channel` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '渠道',
                           `activity_id` bigint(20) DEFAULT NULL COMMENT '活动id',
                           `sex` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '1 男 0 女',
                           `age` int(11) DEFAULT NULL COMMENT '年龄',
                           `weixin` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '微信',
                           `qq` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'qq',
                           `level` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '意向等级',
                           `subject` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '意向学科',
                           `status` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '状态(已分配1  进行中2  回收3  伪线索4)',
                           `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
                           `create_by` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
                           `false_count` int(11) DEFAULT '0' COMMENT '伪线索失败次数(最大数3次)',
                           `next_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                           `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
                           `transfer` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '是否转派',
                           `end_time` timestamp NULL DEFAULT NULL COMMENT '线索失效时间',
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='线索';

/*Data for the table `tb_clue` */

insert  into `tb_clue`(`id`,`name`,`phone`,`channel`,`activity_id`,`sex`,`age`,`weixin`,`qq`,`level`,`subject`,`status`,`create_time`,`create_by`,`false_count`,`next_time`,`update_time`,`transfer`,`end_time`) values (1,'董云影','15011178675','0',110,NULL,NULL,NULL,NULL,NULL,'0','1','2022-06-04 01:06:46','lifeng',0,NULL,NULL,'0',NULL),(2,'刁风','15011178676','0',110,NULL,NULL,NULL,NULL,NULL,'0','1','2022-06-04 01:06:47','lifeng',1,NULL,'2022-06-04 01:08:50','0','2022-06-07 01:08:51'),(3,'韦克','15011178677','0',110,NULL,NULL,NULL,NULL,'2','0','4','2022-06-04 01:06:47','lifeng',2,NULL,'2022-06-04 01:12:16','0','2022-06-07 01:10:33'),(4,'伍影雄','15011178678','0',110,NULL,NULL,NULL,NULL,'2','0','4','2022-06-04 01:06:47','lifeng',1,NULL,'2022-06-04 01:12:23','0','2022-06-07 01:07:26'),(5,'伏黛梁','15011178679','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(6,'庞琦融','15011178680','0',110,NULL,NULL,NULL,NULL,'0','1','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(7,'安克','15011178681','0',110,NULL,NULL,NULL,NULL,'0','3','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(8,'路珍','15011178682','0',110,NULL,NULL,NULL,NULL,'0','4','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(9,'蔡娅莹','15011178683','0',110,NULL,NULL,NULL,NULL,NULL,'2','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(10,'尤洁菲','15011178684','0',110,NULL,NULL,NULL,NULL,NULL,NULL,'1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(11,'皮淑竹','15011178685','0',110,NULL,NULL,NULL,NULL,NULL,'6','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(12,'余莹行','15011178686','0',110,NULL,NULL,NULL,NULL,NULL,'7','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(13,'胡澜','15011178687','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(14,'蒋家弘','15011178688','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(15,'于琴','15011178689','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(16,'吕瑶','15011178690','0',110,NULL,NULL,NULL,NULL,'0','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(17,'吕锦','15011178691','0',110,NULL,NULL,NULL,NULL,'0','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(18,'阮钊渊','15011178692','0',110,NULL,NULL,NULL,NULL,'0','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(19,'时腾雪','15011178693','0',110,NULL,NULL,NULL,NULL,NULL,'1','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(20,'宋源克','15011178694','0',110,NULL,NULL,NULL,NULL,NULL,'3','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(21,'熊纯','15011178695','0',110,NULL,NULL,NULL,NULL,'0','4','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(22,'宋丹晴','15011178696','0',110,NULL,NULL,NULL,NULL,NULL,'2','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(23,'卢澜倩','15011178697','0',110,NULL,NULL,NULL,NULL,'2',NULL,'1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(24,'施霄锦','15011178698','0',110,NULL,NULL,NULL,NULL,'2','6','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(25,'姚文之','15011178699','0',110,NULL,NULL,NULL,NULL,'3','7','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(26,'危钧','15011178700','0',110,NULL,NULL,NULL,NULL,NULL,'0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(27,'曹妤怡','15011178701','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(28,'章奇华','15011178702','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(29,'窦菁竹','15011178703','0',110,NULL,NULL,NULL,NULL,'2','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(30,'郎芳沫','15011178704','0',110,NULL,NULL,NULL,NULL,'0','0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:26','0','2022-06-07 01:07:27'),(31,'昌姣','15011178705','0',110,NULL,NULL,NULL,NULL,NULL,'0','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:46','0','2022-06-07 01:07:46'),(32,'宋函先','15011178706','0',110,NULL,NULL,NULL,NULL,NULL,'1','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:46','0','2022-06-07 01:07:46'),(33,'沈爱云','15011178707','0',110,NULL,NULL,NULL,NULL,'2','3','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:46','0','2022-06-07 01:07:46'),(34,'褚璧','15011178708','0',110,NULL,NULL,NULL,NULL,'2','4','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:46','0','2022-06-07 01:07:46'),(35,'毕清香','15011178709','0',110,NULL,NULL,NULL,NULL,'2','2','1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:46','0','2022-06-07 01:07:46'),(36,'夏建倩','15011178710','0',110,NULL,NULL,NULL,NULL,'0',NULL,'1','2022-06-04 01:06:47','lifeng',0,NULL,'2022-06-04 01:07:46','0','2022-06-07 01:07:46'),(37,'陶克','15011178711','0',110,NULL,NULL,NULL,NULL,'0','6','1','2022-06-04 01:06:47','lifeng',0,NULL,NULL,'0',NULL),(38,'胡菁德','15011178712','0',110,NULL,NULL,NULL,NULL,'0','7','1','2022-06-04 01:06:47','lifeng',0,NULL,NULL,'0',NULL),(39,'禹建静','15011178713','0',110,NULL,NULL,NULL,NULL,NULL,'0','1','2022-06-04 01:06:47','lifeng',0,NULL,NULL,'0',NULL),(40,'王和','15011178714','0',110,NULL,NULL,NULL,NULL,NULL,'0','4','2022-06-04 01:06:47','lifeng',1,NULL,'2022-06-04 01:08:28','0','2022-06-07 01:07:26');

/*Table structure for table `tb_contract` */

DROP TABLE IF EXISTS `tb_contract`;

CREATE TABLE `tb_contract` (
                               `id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '合同id',
                               `phone` varchar(13) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
                               `contract_no` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '合同编号',
                               `name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '客户姓名',
                               `subject` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '意向学科',
                               `activity_id` bigint(20) DEFAULT NULL COMMENT '活动id',
                               `activity_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '活动名称',
                               `course_id` bigint(20) DEFAULT NULL COMMENT '课程id',
                               `course_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '课程名称',
                               `channel` char(1) COLLATE utf8_bin DEFAULT NULL,
                               `status` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '状态(待审核1，已通过2，已驳回3 全部完成4)',
                               `create_by` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建人',
                               `dept_id` bigint(20) DEFAULT NULL,
                               `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
                               `file_name` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '文件名称',
                               `contract_order` float DEFAULT NULL COMMENT '订单价格',
                               `discount_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '折扣类型',
                               `course_price` float DEFAULT NULL COMMENT '课程价格',
                               `process_instance_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
                               `business_id` bigint(20) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='合同';

/*Data for the table `tb_contract` */

INSERT INTO `tb_contract` VALUES ('49572471968700','15300153000','15300153000','刘备','0',110,NULL,8,NULL,'0','4','lisi',216,'2022-07-06 14:22:45','/huike-crm/2022/06/18/6c6949b1ac964e4e9faa0d6f2e6c4eae.pdf',4250,'课程折扣',5000,NULL,NULL),('49650203324700','15300153001','15300153001','关羽','0',110,NULL,8,NULL,'0','4','lisi',216,'2022-07-06 14:22:45','/huike-crm/2022/06/18/83ce1ce3ab9e4596987c3d371b11e74d.pdf',4250,'课程折扣',5000,NULL,NULL),('49686891580400','15300153002','15300153002','张飞','1',110,NULL,5,NULL,'0','4','lisi',216,'2022-07-06 14:22:45','/huike-crm/2022/06/18/310b631abaa84ee58bf1a66b8f8dd707.pdf',1604.8,'课程折扣',1888,NULL,NULL),('49768185175600','15300153003','15300153003','孙权','0',110,NULL,7,NULL,'0','4','lisi',216,'2022-07-06 14:22:45','/huike-crm/2022/06/18/02bec1df48744ce896d50649b1ca6ab7.pdf',8.5,'课程折扣',10,NULL,NULL);

/*Table structure for table `tb_course` */

DROP TABLE IF EXISTS `tb_course`;

CREATE TABLE `tb_course` (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '课程id',
                             `code` varchar(32) COLLATE utf8_bin DEFAULT NULL,
                             `name` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
                             `subject` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '学科',
                             `price` int(11) DEFAULT NULL COMMENT '浠锋牸',
                             `applicable_person` char(1) COLLATE utf8_bin DEFAULT NULL,
                             `info` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '课程描述信息',
                             `create_time` timestamp NULL DEFAULT NULL,
                             `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除 1 是',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='课程管理';

/*Data for the table `tb_course` */

insert  into `tb_course`(`id`,`code`,`name`,`subject`,`price`,`applicable_person`,`info`,`create_time`,`is_delete`) values (1,'3vssua42','UI课程','9',99999,'1','文强文强文强为请问请问','2021-06-03 17:57:35',0),(2,'pddu2qcz','Python','7',1888,'1','新媒体测试新媒体测试','2021-07-15 23:05:50',0),(3,'vk25iaol','大数据','6',5555,'2','','2021-11-05 22:20:22',1),(4,'h9cga9c3','高级测试','6',88888,'2',NULL,'2021-11-06 01:05:50',1),(5,'96u9u5mv','高级前端课程','1',1888,'2','高级前端课程','2021-11-10 13:56:44',0),(6,'p7m47cko','Java高级课程','0',9999,'2','Java高级课程','2021-11-15 14:58:37',0),(7,'b8w71sfy','JAVASE','0',10,'1','适用于0基础人群的java基础课程','2021-11-23 17:20:41',0),(8,'9gozk4jz','JAVAEE','0',5000,'2','javaWEB开发','2021-11-23 17:32:51',0);

/*Table structure for table `tb_rule_pool` */

DROP TABLE IF EXISTS `tb_rule_pool`;

CREATE TABLE `tb_rule_pool` (
                                `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '线程池规则',
                                `limit_time` int(11) DEFAULT NULL COMMENT '回收时限',
                                `limit_time_type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '回收时限字典',
                                `warn_time` int(11) DEFAULT NULL COMMENT '警告时间',
                                `warn_time_type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '警告时间字典单位类型',
                                `repeat_get_time` int(11) DEFAULT NULL COMMENT '重复捞取时间',
                                `repeat_type` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '重复捞取时间字典',
                                `max_nunmber` int(11) DEFAULT NULL COMMENT '最大保有量',
                                `type` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '0 线索 1 商机',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='线索池规则';

/*Data for the table `tb_rule_pool` */

insert  into `tb_rule_pool`(`id`,`limit_time`,`limit_time_type`,`warn_time`,`warn_time_type`,`repeat_get_time`,`repeat_type`,`max_nunmber`,`type`) values (1,3,'1',1,'0',1,'1',30,'0'),(2,3,'0',1,'0',1,'1',70,'1');

DROP TABLE IF EXISTS `tb_clue_track_record`;
CREATE TABLE `tb_clue_track_record`
(
    `id`          bigint PRIMARY KEY AUTO_INCREMENT COMMENT '主键id',
    `clue_id`     bigint NOT NULL COMMENT '线索id',
    `level`       char(1)      DEFAULT NULL COMMENT '意向等级',
    `subject`     char(1)      DEFAULT NULL COMMENT '意向学科',
    `record`      varchar(512) DEFAULT NULL COMMENT '跟进记录',
    `next_time`   datetime     DEFAULT NULL COMMENT '下次跟进时间',
    `create_by`   varchar(32) NOT NULL COMMENT '跟进人',
    `create_time` datetime COMMENT '跟进时间',
    `type`         char(1) DEFAULT '0' COMMENT '0 正常跟进记录 1 伪线索',
    `false_reason` char(1) DEFAULT NULL COMMENT '伪线索原因'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin COMMENT ='线索跟进记录';


DROP TABLE IF EXISTS `tb_business_track_record`;
CREATE TABLE `tb_business_track_record`
(
    `id`           bigint PRIMARY KEY AUTO_INCREMENT COMMENT '主键id',
    `business_id`  bigint COMMENT '商机id',
    `track_status` char(1)      DEFAULT NULL COMMENT '跟进状态',
    `next_time`    datetime     DEFAULT NULL COMMENT '下次跟进时间',
    `key_items`    varchar(32)  DEFAULT NULL COMMENT '沟通重点',
    `record`       varchar(512) DEFAULT NULL COMMENT '沟通纪要',
    `create_by`    varchar(32) COMMENT '跟进人',
    `create_time`  datetime COMMENT '跟进时间',
    `type`         char(1) DEFAULT '0' COMMENT '0 正常跟进记录 1 退回公海',
    `false_reason` char(1) DEFAULT NULL COMMENT '退回公海原因'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin COMMENT ='商机跟进记录';

